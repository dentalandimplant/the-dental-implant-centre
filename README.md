Dr Kash Ubhi qualified from King College, London in 1985. He was one of the first to be awarded a Master of Science Degree in Implant Dentistry at Guys and St Thomas’ Hospital in London in 2000 for which he received a distinction. He was awarded the “Specialist” Status in 2003.

Address: 51 Hill Avenue, Amersham, Buckinghamshire HP6 5BX

Phone: +44 1494 854053
